## 驰骋低代码开发平台

#### 介绍
1. 啥也别说，先看视频教程：https://www.bilibili.com/video/BV1eU4y137XU
1. 驰骋低代码开发平台=流程引擎+表单引擎+组织结构+门户权限+菜单场景体系
1. 该项目是从驰骋BPM发展而来，在其基础上增加了菜单权限管理、表单自定义、场景应用体系等。
1. 工作流引擎的文档：http://doc.ccbpm.cn 
1. 在线演示: http://demo.ccbpm.cn 用户名: admin 密码:123
1. 帮我们点个Star 

#### 软件架构
1. 驰骋低代码开发平台=流程引擎+表单引擎+组织结构+门户权限+菜单场景体系
1. 支持MySQL、oracle、sqlserver、db2、infomix、dm、pgsql、人大金仓等数据库。
1. 支持 .net、java 两大系列语言。
1. 支持单组织版、集团版、SAAS版。

#### 安装教程

1.  下载源代码。
2.  使用VS2019 + 以上打开它。
3.  创建一个空白的数据库，登录用户名必须有超级用户权限，字段不区分大小写。
4.  修改 web.config 数据库连接与相关的配置项。

```
<!-- 数据库链接 -->
<add key="AppCenterDSN" value="Password=xxx;Persist Security Info=True;User ID=sa;Initial Catalog=CCFast0715;Data Source=.;Timeout=999;MultipleActiveResultSets=true" />
<!-- 数据库类型, MySQL,Oracle,DM,PGSQL ....  -->
<add key="AppCenterDBType" value="MSSQL" />
```
5.  ctrl+f5执行运行，系统就进入安装页面, 点击接受协议并安装即可，大概3-5分钟。

### 联系我们

 ** 北京.**  Addr: 北京市.海淀区.创客小镇. Tel: 范经理, 18678299759
 ** 济南.**  Addr: 济南市.高新区.碧桂园凤凰国际F19. Tel: 周经理, 18660153393(微信同号) 
 
## 后台设计
1. 用来设计菜单、实体、台账、表单。
2. 设计流程。
### 菜单设计器
![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/071126_ca725c22_358162.png "屏幕截图.png")

### 实体组件设计器
![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/071625_cd6c52b8_358162.png "屏幕截图.png")

### 集合组件设计器
![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/071739_2ee40f59_358162.png "屏幕截图.png")

### 流程设计器
![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/071319_269576e5_358162.png "屏幕截图.png")

### 表单设计器1
![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/071405_e1f80e06_358162.png "屏幕截图.png")

### 表单设计器2
![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/071445_b6143d37_358162.png "屏幕截图.png")

## 前台运行
### 实体集合管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/071857_3a247b11_358162.png "屏幕截图.png")
### 实体卡片
![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/072233_e5599d72_358162.png "屏幕截图.png")

### 实体执行方法
![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/072349_d420779e_358162.png "屏幕截图.png")
### 实体：二维码
![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/072439_c7ca64e8_358162.png "屏幕截图.png")
### 实体日常记录
![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/072516_056a3b90_358162.png "屏幕截图.png")

### 数据快照
![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/072708_a1740589_358162.png "屏幕截图.png")
### 实体相关的流程发起
![输入图片说明](https://images.gitee.com/uploads/images/2021/0717/071942_8a0b0c66_358162.png "屏幕截图.png")


### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
 